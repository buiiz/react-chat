import { Chat } from "./components/Chat";

function App() {
  return (
    <div className="md:px-1">
      <Chat url='https://edikdolynskyi.github.io/react_sources/messages.json' />
    </div>
  );
}

export default App;
