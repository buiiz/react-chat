import { FC, useEffect, useState } from 'react'
import { MessageType } from '../types/common'
import { Header } from './Header'
import { MessageInput } from './MessageInput'
import { MessageList } from './MessageList'
import { Preloader } from './Preloader'

interface Props {
  url: string;
}

export const Chat: FC<Props> = ({ url }) => {
  const [messages, setMessages] = useState<MessageType[]>([]);
  const [inputMode, setInputMode] = useState<'append' | 'update'>('append');
  const [messageId, setMessageId] = useState<string>('');
  const [inputText, setInputText] = useState('');

  const [userId, setUserId] = useState(''); // TODO: replace userId later
  const avatar = `https://avatars.dicebear.com/api/avataaars/${userId}.svg`; // TODO: replace avatar later
  const user = 'John Doe' // TODO: replace user later

  useEffect(() => {
    setUserId(String(Math.random() * (10 ** 18)));
  }, [])

  const addMessage = () => {
    if (!inputText) {
      return;
    }

    if (inputMode === 'append') {
      const newMessage: MessageType = {
        id: String(Math.random() * (10 ** 18)), // TODO: replace id later
        text: inputText,
        userId: userId,
        avatar: avatar,
        user: user,
        createdAt: new Date().toISOString(),
        editedAt: '',
      }
      setMessages([...messages, newMessage]);
    }

    if (inputMode === 'update') {
      const message = messages.find(message => message.id === messageId)
      const index = messages.indexOf(message!);
      const updatedMessage: MessageType = {
        ...message!,
        text: inputText,
        editedAt: new Date().toISOString(),
      }
      messages[index] = updatedMessage;
      setMessages(messages);
    }

    setInputText('');
    setInputMode('append')
  }

  const updateMessage = (id: string) => {
    const message = messages.find(message => message.id === id)
    setInputText(message?.text || '');
    setMessageId(id)
    setInputMode('update');
  }

  const removeMessage = (id: string) => {
    setMessages(messages.filter((message) => message.id !== id))
  }

  useEffect(() => {
    fetch(url)
      .then(data => data.json())
      .then(messages => setMessages(messages))
  }, [url])

  return (
    <div className="chat mx-auto p-2 sm:p-4 max-w-4xl w-full h-screen flex flex-col gap-2 sm:gap-4 overflow-hidden border-gray-300 border shadow-2xl rounded bg-gradient-to-r from-blue-100 to-pink-100">
      {messages.length ?
        (<>
          <Header messages={messages} />
          <MessageList messages={messages} currentUserId={userId} updateMessage={updateMessage} removeMessage={removeMessage} />
          <MessageInput addMessage={addMessage} text={inputText} setText={setInputText} mode={inputMode} />
        </>) :
        (<>
          <Preloader />
        </>)
      }
    </div>
  )
}
