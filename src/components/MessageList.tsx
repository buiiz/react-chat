import { FC, Fragment } from 'react'
import { compareDates, formatDate } from '../services/dateService'
import { MessageType } from '../types/common'
import { Message } from './Message'
import { OwnMessage } from './OwnMessage'

interface Props {
  messages: MessageType[];
  currentUserId: string;
  removeMessage: (id: string) => void;
  updateMessage: (id: string) => void;
}

export const MessageList: FC<Props> = ({ messages, currentUserId, updateMessage, removeMessage }) => {
  return (
    <div className="message-list px-1 flex-grow overflow-y-auto">
      {messages.map((message, index) => (
        <Fragment key={message.id}>
          {(!compareDates(new Date(message.createdAt), new Date(messages[index - 1]?.createdAt))) && (
            <div className="messages-divider flex justify-center">
              <span className="px-3 py-1.5 rounded-full bg-opacity-20 bg-gray-400">
                {formatDate(new Date(message.createdAt))}
              </span>
            </div>
          )}
          {(message.userId === currentUserId) ? (
            <OwnMessage message={message} updateMessage={updateMessage} removeMessage={removeMessage} />
          ) : (
            <Message message={message} />
          )}
        </Fragment>
      ))}
    </div>
  )
}
