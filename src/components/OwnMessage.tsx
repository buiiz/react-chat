import { FC } from 'react'
import { MessageType } from '../types/common'

interface Props {
  message: MessageType;
  updateMessage: (id: string) => void;
  removeMessage: (id: string) => void;
}

export const OwnMessage: FC<Props> = ({ message, updateMessage, removeMessage }) => {
  return (
    <div className="own-message pl-14 flex flex-row gap-2 my-4 w-full max-w-full items-end">
      <div className="bg-blue-300 px-2 py-1.5 ml-auto mr-0 flex flex-col items-end rounded-tl-xl rounded-tr-xl rounded-bl-xl shadow-lg">
        <div className="message-user-name font-semibold mb-1">{message.user}</div>
        <div className="message-text">{message.text}</div>
        <div className="self-end flex gap-2">
          <time className="message-time font-thin text-sm">{new Date(message.createdAt).toLocaleTimeString('uk', { timeStyle: 'short' })}</time>
          <button
            onClick={() => updateMessage(message.id)}
            className="message-edit text-gray-600"
          >
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
              <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
              <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
            </svg>
          </button>
          <button
            onClick={() => removeMessage(message.id)}
            className="message-delete text-gray-600"
          >
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
              <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
              <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
            </svg>
          </button>
        </div>
      </div>
      <img
        className="message-user-avatar h-12 w-12 rounded-full flex-shrink-0 border border-blue-900"
        src={message.avatar}
        alt={`${message.user} img`}
      />
    </div>
  )
}
