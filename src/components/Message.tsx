import { FC, useState } from 'react'
import { MessageType } from '../types/common'

interface Props {
  message: MessageType;
}

export const Message: FC<Props> = ({ message }) => {
  const [isLiked, setIsLiked] = useState(false);

  return (
    <div className="message pr-14 flex flex-row gap-2 my-4 w-full max-w-full items-end">
      <img
        className="message-user-avatar h-12 w-12 rounded-full flex-shrink-0 border border-pink-900"
        src={message.avatar}
        alt={`${message.user} img`}
      />
      <div className="bg-pink-300 px-2 py-1.5 flex flex-col rounded-tl-xl rounded-tr-xl rounded-br-xl shadow-lg">
        <div className="message-user-name font-semibold mb-1">{message.user}</div>
        <div className="message-text">{message.text}</div>

        <div className="self-end flex gap-2">
          <time className="message-time font-thin text-sm">{new Date(message.createdAt).toLocaleTimeString('uk', { timeStyle: 'short' })}</time>
          <button
            onClick={() => { setIsLiked(value => !value) }}
            className={isLiked ? "message-liked" : "message-like"}
          >
            {isLiked ? (
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" className="bi bi-heart-fill" viewBox="0 0 16 16">
                <path fillRule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
              </svg>
            ) : (
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" className="bi bi-heart" viewBox="0 0 16 16">
                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
              </svg>
            )}
          </button>
        </div>
      </div>
    </div >
  )
}
