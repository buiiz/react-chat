import { FC } from 'react'
import { MessageType } from '../types/common'

interface Props {
  messages: MessageType[];
}

export const Header: FC<Props> = ({ messages }) => {
  const messagesAmount = messages.length;
  const usersAmount = new Set(messages.map((message) => message.userId)).size;
  const lastMessageDate = messages
    .map((message) => new Date(message.createdAt))
    .reduce((maxDate, date) => date > maxDate ? date : maxDate);
  const lastMessageDateString = `${lastMessageDate.toLocaleString('uk', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  })} ${lastMessageDate.toLocaleString('uk', {
    hour: '2-digit',
    minute: '2-digit',
  })}`

  return (
    <header className="header h-12 flex flex-row items-center gap-6 border-gray-200">
      <h2 className="header-title font-bold">
        <span>{'My Chat'}</span>
      </h2>
      <div className="header-users-count flex items-center">
        <span>{usersAmount}</span>
        <span className="ml-1">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-person" viewBox="0 0 16 16">
            <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
          </svg>
        </span>
      </div>
      <div className="header-messages-count flex items-center">
        <span>{messagesAmount}</span>
        <span className="ml-1">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-envelope" viewBox="0 0 16 16">
            <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z" />
          </svg>
        </span>
      </div>
      <div className="ml-auto font-light text-gray-700">
        <span className="hidden sm:inline">Last message at </span>
        <time className="header-last-message-date font-normal">
          {lastMessageDateString}
        </time>
      </div>
    </header>
  )
}
