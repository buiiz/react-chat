import { FC } from 'react'

export const Preloader: FC = () => {
  return (
    <div className="preloader"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
  )
}
