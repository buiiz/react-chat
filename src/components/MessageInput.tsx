import { ChangeEvent, FC, FormEvent } from 'react'

interface Props {
  text: string;
  mode: string;
  setText: (value: string) => void;
  addMessage: () => void,
}

export const MessageInput: FC<Props> = ({ text, setText, addMessage, mode }) => {
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    addMessage();
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setText(e.currentTarget.value)
  }

  return (
    <form
      onSubmit={handleSubmit}
      className="message-input w-full flex flex-row gap-2 items-center h-10 rounded-xl"
    >
      <input
        type="text"
        value={text}
        onChange={handleChange}
        className="message-input-text h-full w-full rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 text-base outline-none py-1 px-3 leading-7 transition-colors duration-200 ease-in-out" />
      <button
        type="submit"
        className="message-input-button h-full flex items-center flex-shrink-0 outline-none bg-blue-500 hover:bg-blue-700 focus:ring-2 text-white py-1.5 px-3 rounded transition-colors duration-200 ease-in-out"
      >
        <span className="mr-1 hidden sm:inline">{mode === 'append' ? 'Send' : 'Update'}</span>
        <span>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-return-left" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M14.5 1.5a.5.5 0 0 1 .5.5v4.8a2.5 2.5 0 0 1-2.5 2.5H2.707l3.347 3.346a.5.5 0 0 1-.708.708l-4.2-4.2a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 8.3H12.5A1.5 1.5 0 0 0 14 6.8V2a.5.5 0 0 1 .5-.5z" />
          </svg>
        </span>
      </button>
    </form>
  )
}
